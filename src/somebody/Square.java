package somebody;

public class Square extends Figure  {
	private static String name = "�������";  

    private double side;
    
    public Square(double side) {
        this.side = side;
    }
    @Override
    public double getArea() {
        return Math.pow(side, 2) ;
    }    
    @Override
    public String getName() {
        return name;
    }    
    public double getside() {
        return side;
    }
    public void setside(double side) {
        this.side = side;
    }
    
}
